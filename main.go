package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

var scanner = bufio.NewScanner(os.Stdin)
var nameCreatedFile = "MyPhoneBook.txt"
var choice string
var todo string = "Выберите желаемое действие:\n \t1. Создать новую запись \n \t2. Показать список записей\n \t3. Найти существующую запись\n \t4. Удалить существующую запись\n \t5. Обновить существующую запись\n \t6. Выход\n \n"
var next string = "Нажмите 7+Enter для просмотра возможных дальнейших действий или 6 для завершения работы"

func main() {

	fmt.Println(todo)
	for {
		fmt.Scanln(&choice)
		switch choice {
		case "1":
			addRecordByPhone()
			fmt.Println(next)
		case "2":
			viewAllRecord()
			fmt.Println(next)
		case "3":
			findRecordByPhone()
			fmt.Println(next)
		case "4":
			deleteRecordByPhone()
			fmt.Println(next)
		case "5":
			updateNameByPhone()
			fmt.Println(next)
		case "6":
			os.Exit(0)
		default:
			fmt.Println(todo)
		}
	}
}

func updateNameByPhone() {
	var key string
	var name string
	fmt.Print("Для изменения существующей записи введите номер телефона в формате +380501234567: ")
	fmt.Scanln(&key)
	file, _ := ioutil.ReadFile(nameCreatedFile)
	lines := strings.Split(string(file), "\n")
	for i, line := range lines {
		if strings.Split(line, " ")[0] == key {
			fmt.Print("Введите новое имя ")
			fmt.Scanln(&name)
			lines[i] = key + " " + name
			toFile := strings.Join(lines, "\n")
			ioutil.WriteFile(nameCreatedFile, []byte(toFile), 0644)
			fmt.Println("Существующая запись обновлена:  " + lines[i])
			return
		}
	}
	fmt.Println("Искомая запись отсутствует ")

}
func deleteRecordByPhone() {
	var key string
	fmt.Print("Для удаления существующей записи введите номер телефона в формате +380501234567: ")
	fmt.Scanln(&key)
	file, _ := ioutil.ReadFile(nameCreatedFile)

	lines := strings.Split(string(file), "\n")
	for i, line := range lines {
		if strings.Split(line, " ")[0] == key {
			lines[i] = lines[len(lines)-1]
			lines = lines[:len(lines)-1]
			fmt.Println("Запись под номером " + key + " удалена успешно")
			toFile := strings.Join(lines, "\n")
			ioutil.WriteFile(nameCreatedFile, []byte(toFile), 0644)
			return
		}
	}
	fmt.Println("Искомая запись отсутствует ")

}

func findRecordByPhone() {
	var key string
	fmt.Print("Для поиска существующей записи введите номер телефона в формате +380501234567: ")
	fmt.Scanln(&key)
	file, _ := ioutil.ReadFile(nameCreatedFile)
	lines := strings.Split(string(file), "\n")
	for _, line := range lines {
		if strings.Split(line, " ")[0] == key {
			fmt.Println(line)
			return
		}

	}
	fmt.Println("Искомая запись отсутствует")
}
func addRecordByPhone() {
	var key string
	var value string
	fmt.Print("Для добавления новой записи введите номер телефона в формате +380501234567: ")
	fmt.Scanln(&key)
	if !checkValidNumber(key) {
		fmt.Println("Введенный номер не соответствует требуемому формату, нажмите 1 для повторного ввода номера или ")
		return
	}
	if !checkAvialableNumberInFile(key) {
		fmt.Print("Введите Имя:")
		fmt.Scanln(&value)
		var records = key + " " + value + "\n"
		file, _ := os.OpenFile(nameCreatedFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
		defer file.Close()
		file.WriteString(records)
	} else {
		fmt.Println("Запись уже существует")
	}
}
func viewAllRecord() {
	file, _ := os.Open(nameCreatedFile)
	reader := bufio.NewReader(file)
	content, _ := ioutil.ReadAll(reader)
	fmt.Println(string(content))
}

func checkValidNumber(key string) bool {
	var validNumber = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`)
	val := validNumber.MatchString(key)
	return val
}

func checkAvialableNumberInFile(key string) bool {
	file, _ := ioutil.ReadFile(nameCreatedFile)
	lines := strings.Split(string(file), "\n")
	for _, line := range lines {
		if strings.Split(line, " ")[0] == key {
			return true
		}
	}
	return false
}
